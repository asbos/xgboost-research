# pd.set_option("display.max_rows", 1000)
# pd.set_option("display.max_columns", 1000)
from sklearn import preprocessing

from utils import *

train = pd.read_csv('data/train.csv')
test = pd.read_csv('data/test.csv')

#Categorical to
categorical = ["X0",  "X1",  "X2", "X3", "X4",  "X5", "X6", "X8"]
for f in categorical:
    if train[f].dtype == 'object':
        print(f)
        lbl = preprocessing.LabelEncoder()
        lbl.fit(list(train[f].values) + list(test[f].values))
        train[f] = lbl.transform(list(train[f].values))
        test[f] = lbl.transform(list(test[f].values))

usable_columns = list(set(train.columns) - set([ 'y']))

Y_train = train['y'].values
id_test = test['ID'].values

X_train = train[usable_columns]
x_test = test[usable_columns]

x_train, x_valid, y_train, y_valid = train_test_split(X_train, Y_train, test_size=0.2, random_state=4242)

d_train = xgb.DMatrix(x_train, label=y_train)
d_valid = xgb.DMatrix(x_valid, label=y_valid)
d_test = xgb.DMatrix(x_test)

params = {}
params['objective'] = 'reg:linear'
params['eta'] = 0.02
params['max_depth'] = 4
# params['seed'] = 42
# params['subsample'] = 1
# params['base_score'] = 0
params['min_child_weight'] = 5

watchlist = [(d_train, 'train'), (d_valid, 'valid')]

clf = xgb.train(params, d_train, 1000, watchlist, early_stopping_rounds=50, feval=xgb_r2_score, maximize=True)

p_test = clf.predict(d_test)

sub = pd.DataFrame()
sub['ID'] = id_test
sub['y'] = p_test
sub.to_csv('output/xgb4242.csv', index=False)

n = -1

alll = clf.get_dump()
model_to_py(0, clf, 'xgb_model.py', alll)

alll.remove(alll[n])
model_to_py(0, clf, 'xgb_model_without_'+str(n)+'.py', alll)
model_to_py(0, clf, 'xgb_model_'+str(n)+'_tree.py', [alll[n]])

LR_train = split_by_leaf(x_train, y_train, for_predict=False)

LR_valid = split_by_leaf(x_valid, y_valid, for_predict=False)

leafs = train()

# valid predict
y_pred, y_true = predict_model(for_kaggle=False)
mean_absolute_error(y_true, y_pred)
r2_score(y_true, y_pred)

# x_test predict for kaggle
LR_test = split_by_leaf(x_test, None, for_predict=True)
y_pred = predict_model(for_kaggle=True)
pars = pd.DataFrame()

pars['ID'] = id_test
pars['y'] = y_pred.sort_index()
pars.to_csv('output/xgb_parser50.csv', index=False)