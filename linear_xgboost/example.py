# pd.set_option("display.max_rows", 1000)
# pd.set_option("display.max_columns", 1000)

import pandas as pd
import xgboost as xgb
from xgboost import XGBRegressor
from lightgbm import LGBMRegressor
from LinearXGBoost import LinearXGBoostRegression
from sklearn import preprocessing
from sklearn.linear_model import LinearRegression, Ridge, HuberRegressor, Lasso, LassoLars, ElasticNet
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from utils import *

train = pd.read_csv('data/train.csv')
test = pd.read_csv('data/test.csv')

# Categorical to
categorical = ["X0", "X1", "X2", "X3", "X4", "X5", "X6", "X8"]
for f in categorical:
    if train[f].dtype == 'object':
        print(f)
        lbl = preprocessing.LabelEncoder()
        lbl.fit(list(train[f].values) + list(test[f].values))
        train[f] = lbl.transform(list(train[f].values))
        test[f] = lbl.transform(list(test[f].values))

def xgb_r2_score(preds, dtrain):
    labels = dtrain.get_label()
    return 'r2', r2_score(labels, preds)

usable_columns = list(set(train.columns) - set(['y']))

y_train = train['y'].values
id_test = test['ID'].values

x_train = train[usable_columns]
x_test = test[usable_columns]

x_train, x_valid, y_train, y_valid = train_test_split(x_train, y_train, test_size=0.2, random_state=4242)

d_train = xgb.DMatrix(x_train, label=y_train)
d_valid = xgb.DMatrix(x_valid, label=y_valid)
d_test = xgb.DMatrix(x_test)

print(x_train.shape)

params = {}
params['objective'] = 'reg:linear'
params['eta'] = 0.02
params['max_depth'] = 4

watchlist = [(d_train, 'train'), (d_valid, 'valid')]

regxgb = xgb.train(params, d_train, 1000, watchlist, early_stopping_rounds=50, feval=xgb_r2_score, maximize=True)

p_test = regxgb.predict(d_test)
sub = pd.DataFrame()
sub['ID'] = id_test
sub['y'] = p_test
sub.to_csv('xgb_295.csv', index=False)

# pre-trained
model = LinearXGBoost(2, regxgb, ElasticNet(), 50, 10000)
# HuberRegressor(epsilon=3, alpha=10000)
# model.setshape(x_train)
num = model.get_leaf_stat(x_train, y_train)
model.fit(x_train, y_train, num)

pred_kaggle = model.predict(x_test)
sub1 = pd.DataFrame()
sub1['ID'] = id_test
sub1['y'] = pred_kaggle

sub1.to_csv('LinearXGB.csv', index=False)