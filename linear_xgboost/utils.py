import re
from collections import Counter

import numpy as np
import pandas as pd
import xgb_model_n_tree
import xgboost as xgb
from sklearn.metrics import mean_absolute_error
# pd.set_option("display.max_rows", 1000)
# pd.set_option("display.max_columns", 1000)
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split


def xgb_r2_score(preds, dtrain):
    """
    :param preds:
    :param dtrain:
    :return:
    """
    labels = dtrain.get_label()
    return 'r2', r2_score(labels, preds)


def xgb_mae_score(preds, dtrain):
    """

    :param preds:
    :param dtrain:
    :return:
    """
    labels = dtrain.get_label()
    return 'mae', mean_absolute_error(labels, preds)


def string_parser(s):
    """

    :param s:
    :return:
    """
    if len(re.findall(r":leaf=", s)) == 0:
        out = re.findall(r"[\w.-]+", s)
        tabs = re.findall(r"[\t]+", s)
        if (out[4] == out[8]):
            missing_value_handling = (" or np.isnan(x['" + out[1] + "']) ")
        else:
            missing_value_handling = ""

        if len(tabs) > 0:
            return (re.findall(r"[\t]+", s)[0].replace('\t', '    ') +
                    '        if state == ' + out[0] + ':\n' +
                    re.findall(r"[\t]+", s)[0].replace('\t', '    ') +
                    '            state = (' + out[4] +
                    ' if ' + "x['" + out[1] + "']<" + out[2] + missing_value_handling +
                    ' else ' + out[6] + ')\n')

        else:
            return ('        if state == ' + out[0] + ':\n' +
                    '            state = (' + out[4] +
                    ' if ' + "x['" + out[1] + "']<" + out[2] + missing_value_handling +
                    ' else ' + out[6] + ')\n')
    else:
        out = re.findall(r"[+-]?\d+(?:\.\d+)?(?:[eE][+-]?\d+)?", s)
        return (re.findall(r"[\t]+", s)[0].replace('\t', '    ') +
                '        if state == ' + out[0] + ':\n    ' +
                re.findall(r"[\t]+", s)[0].replace('\t', '    ') +
                '        return ' + out[1] + '\n')


def tree_parser(tree, i):
    """

    :param tree:
    :param i:
    :return:
    """
    if i == 0:
        return ('    if num_booster == 0:\n        state = 0\n'
                + "".join([string_parser(tree.split('\n')[i])
                           for i in range(len(tree.split('\n')) - 1)]))
    else:
        return ('    elif num_booster == ' + str(i) + ':\n        state = 0\n'
                + "".join([string_parser(tree.split('\n')[i])
                           for i in range(len(tree.split('\n')) - 1)]))


def model_to_py(base_score, out_file, trees):
    """
    save model to py file

    :param base_score: initial score for predict (default=0)
    :param out_file: file.py for new model
    :param trees: string dump of model
    :return:
    """
    result = ["import numpy as np\n\n"
              + "def xgb_tree(x, num_booster):\n"]

    for i in range(len(trees)):
        result.append(tree_parser(trees[i], i))

    with open(out_file, 'w') as the_file:
        the_file.write("".join(result) + "\ndef xgb_predict(x):\n    predict = "
                       + str(base_score) + "\n"
                       + "# initialize prediction with base score\n"
                       + "    for i in range("
                       + str(len(trees))
                       + "):\n        predict = predict + xgb_tree(x, i)"
                       + "\n    return predict")


def model_predict(model, x):
    """
    predict dumped tree
    :param model: selecting tree
    :param x: data for predicting
    :return: predictions
    """
    prediction = []
    for index, row in x.iterrows():
        prediction.append(model.xgb_predict(row))
    return np.float64(prediction)


def split_by_leaf_for_training(x, y, model):
    """
    Split training data by leafs of selecting tree.
    :param x {dataframe}: train data
    :param y {numpy.ndarray}: train labels
    :return: LR {dict}: keys - leafs of selecting tree, values - dataframe of training data with all features and
    columns: leafs,	LR_labels - new labels for linear training, y - old labels.
    """
    y = pd.Series(y, name='y', index=x.index)
    y_2 = pd.Series(y - (model.predict(xgb.DMatrix(x)) - model_predict(xgb_model_n_tree, x)), name='LR_labels', index=x.index)
    n_tree_predict = pd.Series(model_predict(xgb_model_n_tree, x), name='leafs', index=x.index)
    leaf_labels = Counter(n_tree_predict).keys()
    df = pd.concat([x, n_tree_predict, y_2, y], axis=1)
    LR = {}
    for i in leaf_labels:
        LR[i] = df[df['leafs'] == i]
    return LR


def split_by_leaf_for_predict(x, tree_model=xgb_model_n_tree):
    """
    Split testing data by leafs of selecting tree.
    :param x:
    :return: LR {dict}: keys - leafs of selecting tree, values - dataframe of testing data with all features and
    column: leafs.
    """
    n_tree_predict = pd.Series(model_predict(tree_model, x), name='leafs', index=x.index)
    leaf_labels = Counter(n_tree_predict).keys()
    df = pd.concat([x, n_tree_predict], axis=1)
    LR = {}
    for i in leaf_labels:
        LR[i] = df[df['leafs'] == i]
    return LR


def train_model(LR_train, linear_model, min_child, max_child):
    """
    Training the linear model for leaf of selecting tree.
    :param LR_train: {dict} keys - leafs of selecting tree, values - dataframe of training data with all
    features and columns: leafs, LR_labels - new labels for linear training, y - old labels.
    :param linear_model: linear model from sklearn for training leaf sets.
    :param min_child: min count of leaf set.
    :param max_child: max count of leaf set.
    :return: leafs: {list} list of leafs, where r2_score > 0.
    :return: reg: {dict} linear model of leafs.
    """
    reg = {}
    leafs = []
    for i in LR_train.keys():
        if (LR_train[i].shape[0] > min_child) & (LR_train[i].shape[0] < max_child):
            x_train_set, x_valid_set, y_train_set, y_valid_set = \
                train_test_split(LR_train[i].drop(['LR_labels', 'leafs','y'], axis=1), LR_train[i]['LR_labels'],
                                 test_size=0.3, random_state=42)
            reg[i] = linear_model.fit(x_train_set, y_train_set)
            pred_set = reg[i].predict(x_valid_set)
            if r2_score(y_valid_set, pred_set) > 0:
                print('R2>0')
                leafs.append(i)
    return leafs, reg


def predict_model(LR_test, leafs, reg, model):
    """
    predict for test data
    :param LR_test: split by leafs test data
    :param leafs:
    :param reg: trained linear model
    :param model: trained XGBoost
    :return:
    """
    y_pred = pd.Series()
    for i in LR_test.keys():
        if i in leafs:
            pred_without_n = pd.Series(model.predict(xgb.DMatrix(LR_test[i].drop(['leafs'], axis=1))) -
                                       model_predict(xgb_model_n_tree, LR_test[i].drop(['leafs'], axis=1)),
                                       index=LR_test[i].index)
            pred_n = pd.Series(reg[i].predict(LR_test[i].drop(['leafs'], axis=1)), index=LR_test[i].index)
            pred = pred_without_n + pred_n
        else:
            pred = pd.Series(model.predict(xgb.DMatrix(LR_test[i].drop(['leafs'], axis=1))), index=LR_test[i].index)
        y_pred = y_pred.append(pred)
    return y_pred


def model_parser(model, n):
    """
    construct new models by string model dump.
    :param model: XGBoost,
    :param n: tree number for piecewise linear refinement,
    :return: save model.py in disk (TODO: подумать над внутренним сохранением)
    """
    dump_string = model.get_dump()
    model_to_py(0, 'xgb_model.py', dump_string)
    model_to_py(0, 'xgb_model_n_tree.py', [dump_string[n]])
    dump_string.remove(dump_string[n])
    model_to_py(0, 'xgb_model_without_n_tree.py', dump_string)


def count_set_tree(model, x_train):
    """
    Counting the number of samples in each subset of all decision trees obtained from the training model
    :param x_train: Training data
    :param model: Trained model XGBoost
    :return: print list of the number of samples in each subset (shape of list equal to leaf count in tree)
    """
    dump_string = model.get_dump()
    for i in range(len(dump_string)):
        model_to_py(0, 'trees/xgb_model_'+str(i)+'_tree.py', [dump_string[i]])
        import importlib
        imp = importlib.import_module('trees.xgb_model_'+str(i)+'_tree')
        sets_dict = split_by_leaf_for_predict(x_train, tree_model=imp)
        sets_shapes = [x.shape[0] for x in sets_dict.values()]
        print(str(i)+' tree sets_shapes:', sets_shapes)


def add_stat(LR_train, linear_model, min_child, max_child, number_of_tree):
    """
    Training the linear model for leaf of selecting tree.
    :param LR_train: {dict} keys - leafs of selecting tree, values - dataframe of training data with all
    features and columns: leafs, LR_labels - new labels for linear training, y - old labels.
    :param linear_model: linear model from sklearn for training leaf sets.
    :param min_child: min count of leaf set.
    :param max_child: max count of leaf set.
    :return: leafs: {list} list of leafs, where r2_score > 0.
    :return: reg: {dict} linear model of leafs.
    """
    reg = {}
    tree_stat = pd.DataFrame()
    for i in LR_train.keys():
        leaf_stat = pd.DataFrame({'number_of_tree': 0, 'subset_shape': 0, 'r2_score': 0}, index=[0])
        if (LR_train[i].shape[0] > min_child) & (LR_train[i].shape[0] < max_child):
            x_train_set, x_valid_set, y_train_set, y_valid_set = \
                train_test_split(LR_train[i].drop(['LR_labels', 'leafs', 'y'], axis=1), LR_train[i]['LR_labels'],
                                 test_size=0.3, random_state=42)
            reg[i] = linear_model.fit(x_train_set, y_train_set)
            pred_set = reg[i].predict(x_valid_set)
            leaf_stat['subset_shape'] = LR_train[i].shape[0]
            leaf_stat['r2_score'] = r2_score(y_valid_set, pred_set)
            leaf_stat['number_of_tree'] = number_of_tree
            tree_stat = tree_stat.append(leaf_stat, ignore_index=True)
    print("{}_tree_stat".format(number_of_tree), tree_stat)
    return tree_stat
