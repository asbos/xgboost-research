import xgboost

from utils import model_parser
from utils import predict_model
from utils import split_by_leaf_for_predict
from utils import split_by_leaf_for_training
from utils import train_model
from utils import count_set_tree
from utils import add_stat
import pandas as pd

from sklearn.linear_model import LinearRegression


class LinearXGBoostRegression:
    """docstring"""
    def __init__(self, number_of_tree, model, linear_model=LinearRegression(), min_child=20, max_child=500):
        """
        :param number_of_tree: selecting tree for pre-training.
        :param model: XGBoost with params.
        :param linear_model: linear model from sklearn for training leaf sets.
        :param min_child: min count of leaf set.
        :param max_child: max count of leaf set.
        """
        self.number_of_tree = number_of_tree
        self.model = model
        self.linear_model = linear_model
        self.min_child = min_child
        self.max_child = max_child

    def fit(self, x_train, y_train, num):
        """
        Fit LinearXGBoost model
        :param x_train: Training data
        :param y_train: Target values
        :return: self : returns an instance of self
        """
        model_parser(self.model, num)
        print('model parser')
        LR_train = split_by_leaf_for_training(x_train, y_train, self.model)
        self._leafs, self._reg = train_model(LR_train, self.linear_model, self.min_child, self.max_child)
        print('training finished!')

    def predict(self, x_valid):
        """
        Predict using the LinearXGBoost model
        :param x_valid: Samples
        :return: Returns predicted values
        """
        LR_valid = split_by_leaf_for_predict(x_valid)
        y_pred = predict_model(LR_valid, self._leafs, self._reg, model=self.model)
        return y_pred

    def setshape(self, x_train):
        """
        Counting the number of samples in each subset of all decision trees obtained from the training model
        :param x_train: Training data
        :return: print list of the number of samples in each subset (shape of list equal to leaf count in tree)
        """
        count_set_tree(self.model, x_train)

    def get_tree(self, x_train, y_train):
        """
        Getting a suitable tree for further optimal learning
        :param x_train: Training data
        :param y_train: Target values
        :return: tree_number with positive r2_score or message about to try training
        """
        model_stat = pd.DataFrame()
        for tree_number in range(len(self.model.get_dump())-1, -1, -1):
            print(tree_number)
            model_parser(self.model, tree_number)
            print(tree_number)
            LR_train = split_by_leaf_for_training(x_train, y_train, self.model)
            tree_stat = add_stat(LR_train, self.linear_model, self.min_child, self.max_child, tree_number)
            for i in tree_stat['r2_score']:
                if i > 0:
                    return tree_number
        "Change params in XGBoost and try again"
        return tree_number