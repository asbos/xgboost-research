# LinearXGBoost Package 
LinearXGBoost is a piecewise linear improvement of the [XGBoost package](https://github.com/dmlc/xgboost).

## Main idea
Pre-training XGBoost model by replacing the constants with linear models at the leaf nodes of the decision tree

![alt text](https://gitlab.com/asbos/xgboost-research/uploads/429c0a213c3a1f0e13f9182aeec4c1cc/const.png)

![alt text](https://gitlab.com/asbos/xgboost-research/uploads/4bd091dcfe4a79c564c6e8614c57cd32/leafs.png)

## How to install?
```pip install linear_xgboost```

## Example

```
import xgboost
from LinearXGBoost import LinearXGBoostRegression
from sklearn.linear_model import LinearRegression


# data loading
train = pd.read_csv('data/train.csv')
test = pd.read_csv('data/test.csv')

x_train = train[usable_columns]
x_test = test[usable_columns]
y_train = train['y'].values

x_train, x_valid, y_train, y_valid = train_test_split(x_train, y_train, test_size=0.2, random_state=4242)

d_train = xgb.DMatrix(x_train, label=y_train)
d_valid = xgb.DMatrix(x_valid, label=y_valid)
d_test = xgb.DMatrix(x_test)
watchlist = [(d_train, 'train'), (d_valid, 'valid')]

params = {}
params['objective'] = 'reg:linear'
params['eta'] = 0.02
params['max_depth'] = 4

# XGBoost training
regxgb = xgb.train(params, d_train, 1000, watchlist, early_stopping_rounds=50, feval=xgb_r2_score, maximize=True)

# LinearXGBoost pre-training
model = LinearXGBoostRegression(random_tree_number=2, xgb_model=regxgb, linear_model=LinearRegression(), min_child=50, max_child=1000)
tree_number = model.get_tree(x_train, y_train)
model.fit(x_train, y_train, tree_number)

y_pred = model.predict(x_test)


```



